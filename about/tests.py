from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import landing, about

# Create your tests here.
class AboutUnitTest(TestCase):
    def test_landing_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 302)

    def test_landing_url_using_landing_function(self):
        found = resolve('/')
        self.assertEqual(found.func, landing)

    def test_about_url_is_exist(self):
        response = Client().get('/about/')
        self.assertEqual(response.status_code, 200)

    def test_about_url_using_about_function(self):
        found = resolve('/about/')
        self.assertEqual(found.func, about)

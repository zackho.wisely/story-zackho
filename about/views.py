from django.shortcuts import render,redirect

# Create your views here.
def landing(request):
    return redirect('/about/')
    
def about(request):
    return render(request, 'about.html', {'nbar':'about'})
$(document).ready(function(){
    $('.move').children().click(function (e) {
        var $div = $(this).closest('.card');
        if (jQuery(e.target).is('.move-down')) {
            $div.next('.card').after($div);
        } else {
            $div.prev('.card').before($div);
        }
    });
});
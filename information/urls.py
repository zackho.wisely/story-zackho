from django.urls import path
from information import views

app_name = 'information'

urlpatterns =[
    path('', views.info, name='info'),
]

from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import info

# Create your tests here.
class InfoUnitTest(TestCase):
    def test_info_url_is_exist(self):
        response = Client().get('/informasi/')
        self.assertEqual(response.status_code, 200)

    def test_info_url_using_info_function(self):
        found = resolve('/informasi/')
        self.assertEqual(found.func,info)

    def test_info_use_info_html(self):
        response = Client().get('/informasi/')
        self.assertTemplateUsed('info.html')
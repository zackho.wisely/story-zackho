from django.test import TestCase
from django.test import Client
from django.urls import resolve,reverse 
from .views import kegiatan, tambahkegiatan, tambahpeserta
from .models import Kegiatan, Peserta
from .forms import BuatKegiatanForm, TambahPesertaForm
from .templatetags.kegiatan_extras import peserta_kegiatan
# Create your tests here.
class KegiatanUnitTest(TestCase):
    def test_kegiatan_url_is_exist(self):
        response = Client().get('/kegiatan/')
        self.assertEqual(response.status_code, 200)

    def test_kegiatan_url_using_kegiatan_function(self):
        Kegiatan.objects.create(judul='a')
        Peserta.objects.create( kegiatan=Kegiatan.objects.get(judul='a'), nama='budi')
        response = resolve('/kegiatan/')
        self.assertEqual(response.func, kegiatan)
    
    def test_tambahkegiatan_url_is_exist(self):
        response = Client().get('/kegiatan/tambahkegiatan/')
        self.assertEqual(response.status_code, 302)

    def test_tambahkegiatan_url_using_tambahkegiatan_function(self):
        response = resolve('/kegiatan/tambahkegiatan/')
        self.assertEqual(response.func, tambahkegiatan)

    def test_tambahkegiatan_post_method_success_and_redirect(self):
        response_post = Client().post('/kegiatan/tambahkegiatan/', {'judul' : 'judul'})
        self.assertEqual(response_post.status_code, 302)

    def test_tambahkegiatan_post_method_not_valid_and_redirect(self):
        response_post = Client().post('/kegiatan/tambahkegiatan/')
        self.assertEqual(response_post.status_code, 302)

    def test_tambahpeserta_url_is_exist(self):
        response = Client().get('/kegiatan/tambahpeserta/1')
        self.assertEqual(response.status_code, 302)
    
    def test_tambahkegiatan_url_using_tambahkegiatan_function(self):
        response = resolve('/kegiatan/tambahpeserta/1')
        self.assertEqual(response.func, tambahpeserta)
    
    def test_tambahpeserta_post_method_success_and_redirect(self):
        Kegiatan.objects.create(judul='kegiatan1')
        response_post = Client().post('/kegiatan/tambahpeserta/1', {'nama':'budi'})
        self.assertEqual(response_post.status_code, 302)
    
    def test_tambahpeserta_post_method_not_valid_and_redirect(self):
        Kegiatan.objects.create(judul='kegiatan1')
        response_post = Client().post('/kegiatan/tambahpeserta/1', {'nama' : ''})
        self.assertEqual(response_post.status_code, 302)

    def test_model_Kegiatan_can_create(self):
        Kegiatan.objects.create(judul='kegiatan')
        self.assertEqual(Kegiatan.objects.all().count(), 1)
        self.assertEqual(Kegiatan.objects.get(judul='kegiatan').__str__(), 'kegiatan')
    
    def test_model_Peserta_can_create(self):
        Kegiatan.objects.create(judul='kegiatan')
        Peserta.objects.create(kegiatan=Kegiatan.objects.first(),nama='budi')
        self.assertEqual(Peserta.objects.all().count(), 1)
        self.assertEqual(Peserta.objects.get(nama='budi').__str__(), 'budi')
    
    def test_BuatKegiatanForm_validation_for_not_valid(self):
        form = BuatKegiatanForm(data={'judul' : ''})
        self.assertFalse(form.is_valid())

    def test_BuatKegiatanForm_validation_fort_valid(self):
        form = BuatKegiatanForm(data={'judul' : 'kegiatan1'})
        self.assertTrue(form.is_valid())

    def test_TambahPesertaForm_validation_for_not_valid(self):
        form = TambahPesertaForm(data={'kegiatan' : '', 'nama' :''})
        self.assertFalse(form.is_valid())

    def test_TambahPesertaForm_validation_for_valid(self):
        Kegiatan.objects.create(judul='kegiatan')
        form = TambahPesertaForm(data={'kegiatan' : Kegiatan.objects.first() , 'nama' :'budi'})
        self.assertTrue(form.is_valid())

    def test_custom_template_peserta_kegiatan(self):
        dict ={
            'a' : 1,
            'b' : 2
        }
        self.assertEqual(peserta_kegiatan(dict, 'a'),1)
from django.urls import path
from kegiatan import views

app_name = 'kegiatan'

urlpatterns =[
    path('', views.kegiatan, name='kegiatan'),
    path('tambahkegiatan/', views.tambahkegiatan, name='tambahkegiatan'),
    path('tambahpeserta/<slug:slug>', views.tambahpeserta, name='tambahpeserta'),
]

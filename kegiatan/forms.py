from django.forms import ModelForm, TextInput, HiddenInput
from crispy_forms.helper import FormHelper
from django.utils.translation import gettext_lazy as _
from .models import *

class BuatKegiatanForm(ModelForm):
    class Meta:
        model = Kegiatan
        fields = ['judul']
        labels = {
            'judul' : _('Nama Kegiatan')
        }


class TambahPesertaForm(ModelForm):
    class Meta:
        model = Peserta
        fields = ['nama',]
        widgets = {
            'nama' : TextInput(
                attrs= {'placeholder' : "Nama Peserta"}
            ),
        }
    
    
from django import template
from kegiatan.models import *
register = template.Library()

@register.filter
def peserta_kegiatan(dict, key):
    return dict.get(key)
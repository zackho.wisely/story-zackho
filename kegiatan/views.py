from django.shortcuts import render, redirect
from django.contrib import messages
from .forms import *
from .models import *

# Create your views here.
def kegiatan(request):
    formKegiatan = BuatKegiatanForm()
    formPeserta = TambahPesertaForm()
    kegiatan = Kegiatan.objects.all()
    peserta = Peserta.objects.all()
    peserta_kegiatan = {}

    for setiap_kegiatan in kegiatan:
        list_peserta = Peserta.objects.filter(kegiatan=setiap_kegiatan)
        peserta_kegiatan.update({setiap_kegiatan : list_peserta})

    context = {
        'nbar':'kegiatan', 
        'form_kegiatan': formKegiatan, 
        'form_peserta': formPeserta,
        'kegiatan' : kegiatan,
        'peserta' : peserta,
        'list_peserta' : peserta_kegiatan,
    }
    return render(request, 'kegiatan.html', context)

def tambahkegiatan(request):
    if request.method == 'POST':
        form = BuatKegiatanForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, 'Kegiatan berhasil ditambahkan')
            return redirect('/kegiatan/')
        else:
            return redirect('/kegiatan/')
    return redirect('/kegiatan/')

def tambahpeserta(request, slug):
    if request.method == 'POST':
        form = TambahPesertaForm(request.POST)
        if form.is_valid():
            peserta = form.save(commit=False)
            peserta.kegiatan = Kegiatan.objects.get(id=slug)
            peserta.save()
            messages.success(request, 'Peserta berhasil ditambahkan')
            return redirect('/kegiatan/')
        else:
            return redirect('/kegiatan/')
    else:
        return redirect('/kegiatan/')
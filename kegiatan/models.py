from django.db import models

# Create your models here.
class Kegiatan(models.Model):
    judul = models.CharField(max_length=15)

    def __str__(self):
        return self.judul;

class Peserta(models.Model):
    kegiatan = models.ForeignKey('Kegiatan', on_delete = models.RESTRICT)
    nama = models.CharField(max_length=15)

    def __str__(self):
        return self.nama;
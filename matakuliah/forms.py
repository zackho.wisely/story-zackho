from django.forms import ModelForm, TextInput
from django.utils.translation import gettext_lazy as _
from .models import Matakuliah

class BuatMatkulForm(ModelForm):
    class Meta:
        model = Matakuliah
        fields = ['nama','sks','pengajar','deskripsi','ruangkelas','semester']
        labels = {
            'nama' : _('Nama Mata Kuliah'),
            'sks' : _('SKS'),
            'pengajar' : _('Nama Pengajar'),
            'deskripsi': _('Deskripsi Mata Kuliah'),
            'ruangkelas' : _('Ruang Kelas'),
            'semester' : _('Semester Mata Kuliah'),
        }
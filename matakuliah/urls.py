from django.urls import path
from matakuliah import views

app_name = 'matakuliah'

urlpatterns =[
    path('', views.matakuliah, name='matakuliah'),
    path('tambahmatkul/', views.tambahmatkul, name='tambahmatkul'),
    path('detil/<slug:slug>/', views.detil, name ='detil'),
    path('hapus/<slug:slug>/', views.hapus, name = 'hapus')
]

from django.test import TestCase
from django.test import Client
from django.urls import resolve,reverse 
from .views import matakuliah, tambahmatkul, detil, hapus
from .models import Matakuliah
from .forms import BuatMatkulForm

# Create your tests here.
class MatakuliahUnitTest(TestCase):
    def test_matakuliah_url_exist(self):
        response = Client().get('/matakuliah/')
        self.assertEqual(response.status_code, 200)

    def test_matakuliah_using_matakuliah_function(self):
        response = resolve('/matakuliah/')
        self.assertEqual(response.func, matakuliah)
    
    def test_tambahmatkul_url_exist(self):
        response = Client().get('/matakuliah/tambahmatkul/')
        self.assertEqual(response.status_code, 200)

    def test_tambahmatkul_using_tambahmatkul_function(self):
        response = resolve('/matakuliah/tambahmatkul/')
        self.assertEqual(response.func, tambahmatkul)

    def test_tambahmatkul_post_method_success_and_redirect(self):
        response_post = Client().post('/matakuliah/tambahmatkul/', {'nama':'budi', 'sks':3, 'pengajar':'pak budi', 'deskripsi':'tidak ada', 'ruangkelas':'1', 'semester': '1'})
        self.assertEqual(response_post.status_code, 302)
    
    def test_tambahmatkul_post_method_not_valid_and_redirect(self):
        response_post = Client().post('/matakuliah/tambahmatkul/', {'nama':'', 'sks':3, 'pengajar':'', 'deskripsi':'', 'ruangkelas':'', 'semester': ''})
        self.assertEqual(response_post.status_code, 200)

    def test_detil_url_exist(self):
        Matakuliah.objects.create(nama='budi', sks=3, pengajar='pak budi', deskripsi='tidak ada', ruangkelas='1', semester= '1')
        response = Client().get('/matakuliah/detil/1/')
        self.assertEqual(response.status_code, 200)
    
    def test_detil_using_detil_function(self):
        response = resolve('/matakuliah/detil/1/')
        self.assertEqual(response.func, detil)
    
    def test_hapus_url_exist(self):
        Matakuliah.objects.create(nama='budi', sks=3, pengajar='pak budi', deskripsi='', ruangkelas='1', semester= '1')
        response = Client().get('/matakuliah/hapus/1/')
        self.assertEqual(response.status_code, 302)

    def test_hapus_using_hapus_function(self):
        response = resolve('/matakuliah/hapus/1/')
        self.assertEqual(response.func, hapus)
    
    def test_Matakuliah_model_can_create(self):
        Matakuliah.objects.create(nama='budi', sks=3, pengajar='pak budi', deskripsi='', ruangkelas='1', semester= '1')
        self.assertEqual(Matakuliah.objects.all().count(),1)
        self.assertEqual(Matakuliah.objects.get(nama='budi').__str__(), 'budi')
    
    def test_BuatMatkulForm_form_validation_for_invalid(self):
        form = BuatMatkulForm(data={'nama':'', 'sks':3, 'pengajar':'', 'deskripsi':'', 'ruangkelas':'', 'semester': ''})
        self.assertFalse(form.is_valid())

    def test_BuatMatkulForm_form_validation_for_valid(self):
        form = BuatMatkulForm(data={'nama':'budi', 'sks':3, 'pengajar':'pak budi', 'deskripsi':'', 'ruangkelas':'1', 'semester': '1'})
        self.assertTrue(form.is_valid())
    
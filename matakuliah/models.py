from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator 

# Create your models here.
class Matakuliah(models.Model):
    nama = models.CharField(max_length = 25, )
    sks = models.PositiveIntegerField(default=1, validators=[MinValueValidator(1), MaxValueValidator(10)])
    pengajar = models.CharField(max_length = 25,)
    deskripsi = models.TextField(max_length = 100, blank = True)
    ruangkelas = models.CharField(max_length = 5)
    SEMESTER_MATKUL = [
        ('1', 'Gasal 20/21'),
        ('2', 'Genap 20/21'),
        ('3', 'Gasal 21/22'),
    ]
    semester = models.CharField(max_length=1, choices=SEMESTER_MATKUL, default='Genap 20/21')


    def __str__(self):
        return self.nama
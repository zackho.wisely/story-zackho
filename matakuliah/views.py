from django.shortcuts import render,redirect
from django.contrib import messages
from .forms import BuatMatkulForm
from .models import Matakuliah as matkuliah

# Create your views here.
def matakuliah(request):
    matakuliah = matkuliah.objects.all()

    return render(request, 'matkul.html', {'nbar':'matkul','matkul':matakuliah})

def tambahmatkul(request):
    if request.method == 'POST':
        form = BuatMatkulForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, 'Mata kuliah berhasil ditambahkan')
            return redirect('/matakuliah/')
        else:
            form = BuatMatkulForm()
            return render(request, 'tambahmatkul.html',{'form':form})
    else:
        form = BuatMatkulForm()
        return render(request, 'tambahmatkul.html', {'form':form})

def detil(request, slug):
    detil = matkuliah.objects.get(id=slug)

    return render(request, 'detil.html', {'matkul':detil})

def hapus(request,slug):
    matkul = matkuliah.objects.get(id=slug)
    matkul.delete()
    messages.success(request, 'Mata kuliah berhasil dihapus')
    return redirect('/matakuliah/')
